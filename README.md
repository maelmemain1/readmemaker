# **readmemaker**

## **Description**
- ReadMeMaker est un programme java permettant de générer le readme d'un projet, il créé un squelette type et ajoute les informations qu'il trouve (Nom, Version, Description...).

## **Versions**
- Version actuelle :
  - v1.0 first release

## **Usage**
- Installer Java 17
- Lancer le launcher : 
    - Windows :
      - Lancer le .bat
      - Suivre les instructions : entrer le path de votre projet.
    - MacOs / Linux : 
      - Lancer le sh avec la commande suivante : _bash rdmm.sh_ ou bien _./rdmm.sh_
      - Suivre les instructions : entrer le path de votre projet.

## **Librairies**
- java.util.List
- java.util.ArrayList
- java.util.EnumSet
- java.util.Objects
- java.util.Scanner
- java.util.List
- java.util.ArrayList
- java.io.IOException
- java.io.BufferedReader
- java.nio.file.Path
- java.nio.file.Paths
- java.nio.file.Files
- java.nio.file.FileVisitOption
- java.nio.file.FileVisitResult
- java.nio.file.SimpleFileVisitor
- java.nio.file.attribute.BasicFileAttributes
- java.nio.charset.StandardCharsets

## **Autres**

## **Infos**
- 442 lignes de code
- 2 classes

## **Credits**
- Maël Memain - Février 2024
