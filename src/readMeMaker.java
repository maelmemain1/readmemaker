import java.util.EnumSet;
import java.util.Objects;
import java.util.Scanner;
import java.util.List;
import java.util.ArrayList;
import java.io.IOException;
import java.io.BufferedReader;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.Files;
import java.nio.file.FileVisitOption;
import java.nio.file.FileVisitResult;
import java.nio.file.SimpleFileVisitor;
import java.nio.file.attribute.BasicFileAttributes;
import java.nio.charset.StandardCharsets;

/**
 * readmemaker
 */
public class readMeMaker {

    private static String version = "1.0";
    private static int fichiers = 0;
    // private static String librairies = "";
    private static List<String> librairies = new ArrayList<>();

    public static void main(String[] args) {
        try {
            entree();

        } catch (Exception e) {
            System.exit(1);
        }
    }

    /**
     * Méthode permettant de récupérer l'entree de l'utilisateur pour qu'il puisse
     * entrer le path du projet dans lequel créer le readme.
     */
    private static void entree() {

        System.out.println();
        System.out.println("\033[1;34mreadMeMaker v" + version);
        System.out.println("\033[1;32mMade by Maël Memain");
        System.out.println();

        Scanner sc = new Scanner(System.in);
        String path = "";

        System.out.print(
                "\033[1;34mreadMeMaker -> \033[1;35mQuel est le path du projet où créer le readme.md ? : \033[0m");
        path = sc.nextLine();
        Objects.requireNonNull(path);

        System.out.println();

        try {
            String c = createContent(path);
            createReadMeFile(path, c);

        } catch (Exception e) {
            System.err.println(
                    "\033[1;31mErreur, paramètre invalide -> \033[0m" + e);
            entree();

        } finally {
            sc.close();
        }

    }

    /**
     * 
     * @param path
     * @return
     */
    private static boolean readmeExists(String path) {
        Path readmePath = Paths.get(path, "README.md");
        return readmePath.toFile().exists();
    }

    /**
     * 
     * @param path
     * @return
     * @throws IOException
     */
    private static String createContent(String path) {

        String ret = "";

        try {

            Path projectPath = Paths.get(path);
            String projectName = projectPath.getFileName().toString();

            int lignes = compteLignes(path);

            readMe rm = new readMe(projectName, path);

            if (readmeExists(path)) {
                projectPath = Paths.get(path + "\\README.md");
                rm.setDescription(readExistingReadMe(projectPath, "Description", rm));
                rm.setVersions(readExistingReadMe(projectPath, "Versions", rm));
                rm.setUsage(readExistingReadMe(projectPath, "Usage", rm));
                readExistingReadMe(projectPath, "Librairies", rm);
                rm.setInfos(readExistingReadMe(projectPath, "Infos", rm));
                rm.setCredits(readExistingReadMe(projectPath, "Credits", rm));
                rm.setCredits(readExistingReadMe(projectPath, "Crédits", rm));
                rm.setAutres(readExistingReadMe(projectPath, "?", rm));
                System.out.println("\033[1;34mreadMeMaker -> \033[1;32mscan du readMe existant terminé\033[0m");
            }

            rm.setLibrairies(librairies);
            rm.setInfosLetF("" + lignes, "" + fichiers);

            ret = rm.readMeToString();

        } catch (IOException e) {
            System.err.println(
                    "\033[1;31mErreur, le path est invalide -> \033[0m" + e);
        }

        return ret;
    }

    /**
     * 
     * @param file
     * @param libraries
     * @throws IOException
     */
    private static String readExistingReadMe(Path file, String balise, readMe rm) throws IOException {
        String ret = "";
        try (BufferedReader reader = Files.newBufferedReader(file, StandardCharsets.UTF_8)) {
            String line;
            boolean dansBalise = false;

            while ((line = reader.readLine()) != null) {
                if (line.contains("## **" + balise + "**")
                        || (balise.equals("?") && line.startsWith("## **") && line.endsWith("**")
                                && (!line.contains("Description") && !line.contains("Versions")
                                        && !line.contains("Usage") && !line.contains("Librairies")
                                        && !line.contains("Infos") && !line.contains("Crédits"))
                                && !line.contains("Credits"))) {
                    dansBalise = true;
                } else if (dansBalise && (!line.contains("#"))) {
                    if (balise.equals("Librairies")) {
                        rm.addLibrairie("\n" + line);
                    }
                    System.out.println(line);
                    ret += "\n" + line;
                } else {
                    dansBalise = false;
                }
            }

            System.out.println("\033[1;34mreadMeMaker -> \033[1;33mscan du readMe existant en cours\033[0m");

        } catch (IOException e) {
            System.err.println(
                    "\033[1;31mErreur, lecture du readme existant impossible -> \033[0m" + e);
        }
        return ret;
    }

    /**
     * 
     * @param file
     * @param libraries
     * @throws IOException
     */
    private static void extractLibraries(Path file) throws IOException {
        try (BufferedReader reader = Files.newBufferedReader(file, StandardCharsets.UTF_8)) {
            String line;
            while ((line = reader.readLine()) != null) {
                if (line.startsWith("import") && line.endsWith(";")) {
                    librairies.add("\n- " + line.substring("import".length(), line.length() - 1).trim());
                }
            }
        }
    }

    /**
     * 
     * @param path
     * @return ret
     * @throws IOException
     */
    private static int compteLignes(String path) throws IOException {
        int ret = 0;
        int tmp = fichiers;

        System.out.println("\033[1;34mreadMeMaker -> \033[1;33mscan des fichiers java\033[0m");
        ret = ret + countLinesOfCode(path, ".java");
        System.out.println("\033[1;34mreadMeMaker -> \033[1;33m" + (fichiers - tmp) + " fichier(s) scanné(s)\033[0m");

        tmp = fichiers;
        System.out.println();
        System.out.println("\033[1;34mreadMeMaker -> \033[1;33mscan des fichiers php\033[0m");
        ret = ret + countLinesOfCode(path, ".php");
        System.out.println("\033[1;34mreadMeMaker -> \033[1;33m" + (fichiers - tmp) + " fichier(s) scanné(s)\033[0m");

        tmp = fichiers;
        System.out.println();
        System.out.println("\033[1;34mreadMeMaker -> \033[1;33mscan des fichiers html\033[0m");

        ret = ret + countLinesOfCode(path, ".html");
        System.out.println("\033[1;34mreadMeMaker -> \033[1;33m" + (fichiers - tmp) + " fichier(s) scanné(s)\033[0m");

        tmp = fichiers;
        System.out.println();
        System.out.println("\033[1;34mreadMeMaker -> \033[1;33mscan des fichiers css\033[0m");

        ret = ret + countLinesOfCode(path, ".css");
        System.out.println("\033[1;34mreadMeMaker -> \033[1;33m" + (fichiers - tmp) + " fichier(s) scanné(s)\033[0m");

        tmp = fichiers;
        System.out.println();
        System.out.println("\033[1;34mreadMeMaker -> \033[1;33mscan des fichiers js\033[0m");

        ret = ret + countLinesOfCode(path, ".js");
        System.out.println("\033[1;34mreadMeMaker -> \033[1;33m" + (fichiers - tmp) + " fichier(s) scanné(s)\033[0m");

        tmp = fichiers;
        System.out.println();
        System.out.println("\033[1;34mreadMeMaker -> \033[1;33mscan des fichiers python\033[0m");

        ret = ret + countLinesOfCode(path, ".py");
        System.out.println("\033[1;34mreadMeMaker -> \033[1;33m" + (fichiers - tmp) + " fichier(s) scanné(s)\033[0m");
        tmp = fichiers;
        ret = ret + countLinesOfCode(path, ".ipynb");
        System.out.println("\033[1;34mreadMeMaker -> \033[1;33m" + (fichiers - tmp) + " fichier(s) scanné(s)\033[0m");

        return ret;
    }

    /**
     * 
     * @param path
     * @return
     * @throws IOException
     */
    private static int countLinesOfCode(String path, String suffix) throws IOException {
        final int[] count = { 0 };

        Files.walkFileTree(Paths.get(path), EnumSet.of(FileVisitOption.FOLLOW_LINKS), Integer.MAX_VALUE,
                new SimpleFileVisitor<Path>() {
                    @Override
                    public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException {
                        if (file.toString().endsWith(suffix)) {

                            if (file.toString().endsWith(".java")) {
                                extractLibraries(file);
                            }

                            System.out
                                    .println("\033[1;34mreadMeMaker -> \033[1;32mFichier "
                                            + file.getFileName().toString() + " scanné : "
                                            + "\033[1;35m" + Files.lines(file).count()
                                            + " lignes trouvées\033[0m");
                            count[0] += Files.lines(file).count();
                            fichiers++;
                        }
                        return FileVisitResult.CONTINUE;
                    }
                });

        return count[0];
    }

    /**
     * Méthode permettant de créer une classe java
     * 
     * @param path     le path où créer la classe
     * @param fileName le nom de la classe java
     * @param content  le contenu de la classe java
     * @throws IOException
     */
    private static void createReadMeFile(String path, String content) throws IOException {
        Path filePath = Paths.get(path, "README" + ".md");
        Files.write(filePath, content.getBytes());

        System.out.println();
        System.out.println(
                "\033[1;34mreadMeMaker -> \033[1;32mCréation du README terminée.\033[0m");
    }
}