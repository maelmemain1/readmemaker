
/**
 * readMe
 */
import java.util.List;
import java.util.ArrayList;

public class readMe {
    private String nomProj;
    private String pathProj;
    private String description;
    private String versions;
    private String usage;
    private String infos;
    private String credits;
    private String autres;
    private List<String> librairies;

    /**
     * 
     * @param nom
     * @param path
     */
    public readMe(String nom, String path) {
        this.nomProj = nom;
        this.pathProj = path;
        this.description = "\n- Todo";
        this.versions = "\n- Version actuelle :" + "\n  - v1.0 first release";
        this.usage = "\n- Todo";
        this.credits = "\n- Todo";
        this.infos = "";
        this.autres = "";
        this.librairies = new ArrayList<>();

    }

    // Getter and Setter for nomProj
    public String getNomProj() {
        return nomProj;
    }

    public void setNomProj(String nomProj) {
        this.nomProj = nomProj;
    }

    // Getter and Setter for pathProj
    public String getPathProj() {
        return pathProj;
    }

    public void setPathProj(String pathProj) {
        this.pathProj = pathProj;
    }

    // Getter and Setter for description
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    // Getter and Setter for versions
    public String getVersions() {
        return versions;
    }

    public void setVersions(String versions) {
        this.versions = versions;
    }

    // Getter and Setter for usage
    public String getUsage() {
        return usage;
    }

    public void setUsage(String usage) {
        this.usage = usage;
    }

    // Getter and Setter for librairies
    public List<String> getLibrairies() {
        return librairies;
    }

    public void setLibrairies(List<String> libs) {
        this.librairies = libs;
    }

    /**
     * 
     * @param lib
     */
    public void addLibrairie(String lib) {
        boolean existante = false;
        for (String librairie : this.librairies) {
            if (lib.equals(librairie)) {
                existante = true;
            }
        }
        if (!existante) {
            this.librairies.add(lib);
        }
    }

    // Getter and Setter for infos
    public String getInfos() {
        return infos;
    }

    public void setInfos(String infos) {
        this.infos = infos;
    }

    public void setInfosLetF(String l, String f) {
        if (this.infos.length() < 3 || this.infos.contains("lignes de code")) {
            this.infos = "\n- " + l + " lignes de code" + "\n- " + f + " classes";
        } else {
            this.infos += "\n- " + l + " lignes de code" + "\n- " + f + " classes";
        }
    }

    public void addInfos(String i) {
        this.infos += "\n" + i;
    }

    // Getter and Setter for credits
    public String getCredits() {
        return credits;
    }

    public void setCredits(String credits) {
        this.credits = credits;
    }

    // Getter and Setter for autres
    public String getAutres() {
        return autres;
    }

    public void setAutres(String au) {
        this.autres = au;
    }

    /**
     * 
     * @return
     */
    public String readMeToString() {
        String ret = "";
        ret += "# **" + this.nomProj + "**";
        ret += "\n\n## **Description**";
        ret += this.description;
        ret += "\n\n## **Versions**";
        ret += this.versions;
        ret += "\n\n## **Usage**";
        ret += this.usage;
        ret += "\n\n## **Librairies**";
        for (String lib : this.librairies) {
            ret += lib;
        }
        ret += "\n\n## **Autres**";
        ret += this.autres;
        ret += "\n\n## **Infos**";
        ret += this.infos;
        ret += "\n\n## **Credits**";
        ret += this.credits;

        return ret;
    }
}